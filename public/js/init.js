var selectedObjectID=0;

$(document).ready(function() {
    //var table = $('#sourcesTable').DataTable();


    $('table.display').dataTable( {
        "paging":   false,
        "searching":false,
        "ordering": false,
        "info":     false
    } );

    var table = $('#sourcesTable').DataTable();

    var resultsTable = $('#resultsTable').DataTable();


    $('#sourcesTable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            //selectedObjectID = this.id;
            selectedObjectID = this._DT_RowIndex;
        }
    } );

    $('#deleteRow').click( function () {
        dhtmlx.confirm({
            title: "Удалить",
            cancel: "Отмена",
            type:"confirm",
            text: "Вы уверены, что хотите удалить строку?",
            callback: function(mode) {
                if(mode==true) {
                    table.row('.selected').remove().draw( false );

                    resultsTable.clear().draw();

                    var parameters = { id: selectedObjectID };
                    $.get( '/delete', parameters, function(data) {
                        $('#results').html(data);
                    });
                    dhtmlx.alert("Объект удален!");
                }
            }
        });
    } );


    $('#editRow').click( function () {

        var parameters = { id: selectedObjectID };

        $.get( '/edit', parameters, function(data) {

            $.magnificPopup.open({
                items: {
                    src: 'editForm',
                    type: 'ajax'
                }
            });

        });


    } );






    //Popup
    $('.popup-with-form').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#m_ng'
    });





    $.validator.addMethod("greaterThan", function (value, element, param) {
        var $min = $(param);

        if (this.settings.onfocusout) {
            $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
                $(element).valid();
            });
        }

        return parseInt(value) > parseInt($min.val());
    });

    $.validator.addMethod("lessThan", function (value, element, param) {
        var $min = $(param);

        if (this.settings.onfocusout) {
            $min.off(".validate-lessThan").on("blur.validate-lessThan", function () {
                $(element).valid();
            });
        }

        return parseInt(value) < parseInt($min.val());
    });



    //validate compute
    $("#compute-form").validate({
        rules: {
            hmax_global: {
                required: true,
                max:10,
                min:0.01,
                number: true
            }
        },
        messages: {
            hmax_global: {
                required: "Это обязательное поле",
                max: "Максимальное значение поля: 10",
                min: "Минимальное значение поля: 0.01",
                number: "Вы должны ввести цифры"
            }
        }
    });



    // validate signup form on keyup and submit
    $("#add-form").validate({
        rules: {
            m_ng: {
                required: true,
                lessThan: "#m_kg",
                number: true
            },
            m_kg: {
                required: true,
                greaterThan: "#m_ng",
                number: true
            },
            m_l: {
                required: true,
                min:0.5,
                max:200,
                number: true
            },
            m_d: {
                required: true,
                min:2,
                number: true
            },
            m_dzag: {
                required: true,
                max:200,
                number: true
            },
            m_dtek: {
                required: true,
                number: true
            }
        },
        messages: {
            m_ng: {
                required: "Это обязательное поле",
                lessThan: "Начальная граница должна быть меньше чем конечная",
                number: "Вы должны ввести цифры"
            },
            m_kg: {
                required: "Это обязательное поле",
                greaterThan: "Конечная граница должна быть больше чем начальная",
                number: "Вы должны ввести цифры"
            },
            m_l: {
                required: "Это обязательное поле",
                min:"Минимальное значение поля: 0.5",
                max:"Максимальное значение поля: 200",
                number: "Вы должны ввести цифры"
            },
            m_d: {
                required: "Это обязательное поле",
                min:"Минимальное значение поля: 2",
                number: "Вы должны ввести цифры"
            },
            m_dzag: {
                required: "Это обязательное поле",
                max:"Максимальное значение поля: 200",
                number: "Вы должны ввести цифры"
            },
            m_dtek: {
                required: "Это обязательное поле",
                number: "Вы должны ввести цифры"
            }
        }
    });


} );





function domo(){
    jQuery('#platform-details').html('<code>' + navigator.userAgent + '</code>');

    var elements = ["del", "backspace"];

    // the fetching...
    $.each(elements, function(i, e) { // i is element index. e is element as text.
        var newElement = ( /[\+]+/.test(elements[i]) ) ? elements[i].replace("+","_") : elements[i];

        // Binding keys
        $(document).bind('keydown', elements[i], function assets() {
            var table = $('#sourcesTable').DataTable();

            dhtmlx.confirm({
                title: "Удалить",
                cancel: "Отмена",
                type:"confirm",
                text: "Вы уверены, что хотите удалить строку?",
                callback: function(mode) {
                    if(mode==true) {
                        table.row('.selected').remove().draw( false );
                        var parameters = { id: selectedObjectID };
                        $.get( '/delete', parameters, function(data) {
                            $('#results').html(data);
                        });
                        dhtmlx.alert("Объект удален!");
                    }
                }
            });
            return false;
        });
    });

}

jQuery(document).ready(domo);





