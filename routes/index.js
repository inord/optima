var express = require('express');
var router = express.Router();
var arrayWithData = [];
var arrayWithAnswers = [];
var hmax_global = 0;
var returnCode = 0; //код завершения
var editId=0;
//var editObject = {};

var windowTitle = 'Обточка наружных циллиндрических поверхностей';

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: windowTitle , arrayWithData: arrayWithData, arrayWithAnswers: arrayWithAnswers, hmax: hmax_global, idToEdit: editId });
});


router.get('/editForm', function(req, res) {
    res.render('editForm', { title: windowTitle , arrayWithData: arrayWithData, arrayWithAnswers: arrayWithAnswers, hmax: hmax_global, idToEdit: editId });
});

router.post('/add', function(req, res) {

    var newObj = {};

    if(arrayWithData.length!=0) {
        var prevId = arrayWithData[arrayWithData.length-1].m_id;
        newObj.m_id = parseInt(prevId) + 1;
    } else {
        newObj.m_id = 1;
    }

    newObj.m_ng = req.body.m_ng;
    newObj.m_kg = req.body.m_kg;
    newObj.m_l = req.body.m_l;
    newObj.m_d = req.body.m_d;
    newObj.m_dzag = req.body.m_dzag;
    newObj.m_dtek = req.body.m_dtek;
    arrayWithData[arrayWithData.length] = newObj;


    arrayWithAnswers = [];

    res.render('index', { title: windowTitle, arrayWithData: arrayWithData, arrayWithAnswers: null, hmax: hmax_global, idToEdit: editId });
});

router.get('/edit', function(req, res) {

    editId = req.param("id");

    var objectToEdit = arrayWithData[editId];

    res.render('index', { title: windowTitle, arrayWithData: arrayWithData, arrayWithAnswers: arrayWithAnswers, hmax: hmax_global, idToEdit: editId, editObject: objectToEdit });
});


router.post('/editRow', function(req, res) {


    arrayWithData[editId].m_kg = req.body.m_kg;
    arrayWithData[editId].m_ng = req.body.m_ng;
    arrayWithData[editId].m_l = req.body.m_l;
    arrayWithData[editId].m_d = req.body.m_d;
    arrayWithData[editId].m_dzag = req.body.m_dzag;
    arrayWithData[editId].m_dtek = req.body.m_dtek;

    arrayWithAnswers = [];


    res.render('index', { title: windowTitle, arrayWithData: arrayWithData, arrayWithAnswers: arrayWithAnswers, hmax: hmax_global, idToEdit: editId, editObject: null });
});



router.get('/delete', function(req, res) {

    var idToDelete = req.param("id");

    delete arrayWithData[idToDelete];

    arrayWithData = removeAllNullObjectsFromArray(arrayWithData);

    arrayWithAnswers = [];

    res.render('index', { title: windowTitle, arrayWithData: arrayWithData, arrayWithAnswers: null, hmax: hmax_global, idToEdit: editId });
});



router.get('/compute', function(req, res) {

    hmax_global = req.query.hmax_global;

    optim();

    res.render('index', { title: windowTitle, arrayWithData: arrayWithData, arrayWithAnswers:arrayWithAnswers, hmax: hmax_global, returnCode: returnCode, idToEdit: editId });
});





function removeAllNullObjectsFromArray(objects) {
    var temp = [];
    var i;
    for (i = 0; i < objects.length; ++i) {
        if (objects[i] != null) {
            temp.push(objects[i]);
        }
    }
    return temp
}


//logic

var stageCount;//количество ступеней stageCount = k
var hmax;
var i, j, foundStage; //foundStage = ns
var kp;
var ngobr;
var kgobr;

var lobr = 0;


var dobr;




function optim() {

    //обнуляем переменные внутри функции, иначе не обнуляются
    hmax = hmax_global;

    returnCode=1;
    kp=0;

    while(returnCode==1) {
        optimTask1();
        if(returnCode>1) {
            return;
        }

        optimTask2();
        optimTask3();
    }

}


//Определение ступени которую можно и нужно обработать
function optimTask1() {

    stageCount=arrayWithData.length; //количество ступеней

    returnCode=1;
    foundStage=0;
    ks=stageCount;

    for( i=1; i<=ks; i++ ) {

        if ( arrayWithData[i-1].m_dtek > arrayWithData[i-1].m_d ) {

            if(i==ks) {
                foundStage=i; //Ступень крайняя правая, её всегда можно обработать
                break;
            }

            if ( arrayWithData[i-1].m_dtek - arrayWithData[i].m_dtek > hmax*2 ) {
                foundStage=i; //Ступень крайняя правая, её всегда можно обработать
                break;
            }

        }

    }


    if (foundStage==0) {
        returnCode = 4;
        //ступень, требующая обработки не найдена!
    }

    if(kp==0 && returnCode==2) {
        returnCode=3;
        //Заготовка не требует обработки
    }

}




function optimTask2() {

    h = ( arrayWithData[i-1].m_dtek - arrayWithData[i-1].m_d) * 0.5 ;

    if (h>hmax) {
        h=hmax;
    }

    dobr=arrayWithData[i-1].m_dtek-h*2;
    lobr=0;
    ngobr=arrayWithData[i-1].m_ng;

    for(j=foundStage;j>=1;j--) {

        if(dobr>=arrayWithData[j-1].m_d) {
            lobr=lobr + parseInt(arrayWithData[j-1].m_l);
            kgobr=arrayWithData[j-1].m_kg;
            arrayWithData[j-1].m_dtek=dobr;
        }

    }

}



function optimTask3() {

    kp=kp+1;

    if (kp>50) {
        returnCode=2;
        //"Слишком много переходов",нс, "Увеличьте глубину резания"
    }


    var answerObj = {};
    answerObj.nompr=kp;
    answerObj.nomk=12;
    answerObj.nomng=ngobr;
    answerObj.nomkg=kgobr;
    answerObj.noml=lobr;
    answerObj.nomd=dobr;
    answerObj.nomh=h;

    arrayWithAnswers[arrayWithAnswers.length] = answerObj;

}




module.exports = router;
